# sricks3-gitignore

**!ALERT!** I actually recommend using [gitignore.io](https://gitignore.io) instead. **!ALERT!**

A catch-all .gitignore file based on GitLab's gitignore templates.

Initially, this will just be one giant template which will be a concatenation of many templates and a repository of some of GitLab's existing templates. Later, I hope to create a simple script that will allow a user to select the "sub-templates" s/he wants to have combined into a single .gitignore file.
